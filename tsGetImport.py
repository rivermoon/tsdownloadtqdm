import requests, bs4, subprocess,time
from tkinter import messagebox
from tkinter import *
import os
from tqdm import tqdm
'''videoDownloadTs.py,本程式可以使用'''
'''下載線上影片.ts結尾檔案'''

def main(xLen, xListHead, xListTail, xMP4,xhttpTail,xhttpHead):
    '''主程式'''
    
    '''呼叫各涵式並執行結果'''
    try:
        tailGOT = getTail(xListHead, xListTail,xhttpTail)
        headGOT = getHead(xhttpHead,xhttpTail,xLen)
        countNumGOT = getCountNum(headGOT,tailGOT,xhttpTail)
        lenMiddleGOT = getMiddleLen(xhttpTail,headGOT,tailGOT)
        videoGOTts = getVideo(headGOT,xListHead, countNumGOT,tailGOT, lenMiddleGOT,xListTail,xMP4)
        msg = '正常執行'
        switch = 'ok'
        return msg, videoGOTts, switch
    except:
        msg = '請檢查輸入的網址'
        warning = '執行出現錯誤/下載點有問題/不支援該影片格式'
        switch = ''
        return msg,warning, switch

def getVideo(H,listHeadUrl,counter,T,Mlen,listTailUrl,mp4Name):
    '''自動改變下載位置,下載影片, 並從.ts 轉檔到 .mp4 儲存'''
    lenNum = len(H)
    midFirstIndex = listHeadUrl[lenNum] #變動區塊前端的不變數,通常是0,所以不應該變動
    checkMidFirstIndex = listTailUrl[lenNum] #人工index數字有否對齊檢查用; 正確可用
    midPart = ''
    httpSourceIn = '.\\dorama.ts'
    with open(httpSourceIn,'wb') as f:
        for num in range(counter+1):
            num = str(num)
            lenNum = len(num)
            frontPartIntNum = Mlen - lenNum
            frontPartValue = midFirstIndex * frontPartIntNum
            midPart = frontPartValue + num
            url = H + midPart + T
            # print(url)
            req = requests.get(url,stream=True)
            total_size = int(req.headers['content-length'])
            # with tqdm
            for i in tqdm(iterable=req.iter_content(chunk_size=1024),total=total_size/1024,unit='KB'):
                f.write(i)
            # without tqdm
            # for i in req.iter_content(chunk_size=1024):
            #     f.write(i)
    # subprocess.run(['C:\\Users\\river\\python\\mpeg\\ffmpeg-20200417-889ad93-win64-static\\bin\\ffmpeg','-i',httpSourceIn,mp4Name])
    # if os.path.exists(mp4Name):
    #     os.remove('.\\dorama.ts')
    report = '完成'
    return report

def getMiddleLen(tt,hList,tList):
    '''尋找變動值內值的個數'''
    testWhole = len(tt)
    testHead = len(hList)
    testTail = len(tList)
    return testWhole-testHead-testTail

def getCountNum(headpart,tailPart,tt):
    '''尋找最後一個變數的值,當作迴圈的計數器'''
    fullUrl = tt
    headUrl = len(headpart)
    secondPart = fullUrl[headUrl:]
    middle = secondPart.split('.')
    num = int(middle[0])
    return num

def getTail(hh,tt,strTail):
    '''網址末端不變的的檔案格式剪下來;例如本例子是: .ts'''
    tailList = strTail.split('.')
    tail = '.'+tailList[-1]
    return tail

def getHead(hh,tt,idx):
    '''根據輸入的兩個網址, 把網址前半段不變的部分剪下來'''
    HEAD = ''
    for letter in range(idx):
        if hh[letter] == tt[letter]:
            HEAD += hh[letter]
        else:
            break
    return HEAD